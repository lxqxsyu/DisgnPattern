# 设计模式

## 项目运行说明

方法一：使用Android Studio导入module的方式

方法二：进入application/src/main/java将代码拷贝到jdk环境执行



## 创建型模式

### 简单工厂模式
工厂类是整个模式的关键。它包含必要的判断逻辑，能够根据外界给定的信息知道创建那个类的实例，外部无需了解该对象是如何被创建和组织的。有利于软件体系结构化。由于工厂类集中了所有实例的创建逻辑，简单工厂模式的缺点也体现在工厂类上。

![这里写图片描述](http://img.blog.csdn.net/20171102210927727?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


@简单工厂模式示意图（参考代码pattern1)


### 工厂方法模式
工厂方法模式的意义是定义一个创建产品对象的工厂接口，将实际创建工作推迟到子类当中。核心工厂类不再负责产品的创建，这样核心类成为一个抽象的工厂角色，仅负责具体工厂子类必须实现的接口，这样进一步抽象化的好处是使得工厂方法模式可以使系统在不修改具体工厂角色的情况下引进新的产品。

![这里写图片描述](http://img.blog.csdn.net/20171102211026514?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

@工厂方法模式示意图（参考代码pattern2)


### 抽象工厂模式
抽象工厂模式是所有形态的工厂模式中最为抽象和最具一般性的。抽象工厂模式可以向客户端提供一个接口，使得客户端在不必指定产品的具体类型的情况下，能够创建多个产品族的产品对象。

![这里写图片描述](http://img.blog.csdn.net/20171102211140090?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

@抽象工厂模式示意图（参考代码pattern3)

### 单例模式
单例模式是让一个类只创建一个实例，有“懒汉式”和“饿汉式”两种，有多种写法

**第一种** ：懒汉式线程不安全

	public class Singleton {  
	    private static Singleton instance;  
	    private Singleton (){}  
	  
	    public static Singleton getInstance() {  
	    if (instance == null) {  
	        instance = new Singleton();  
	    }  
	    return instance;  
	    }  
	}  

**第二种**：懒汉式线程安全

	public class Singleton {  
	    private volatile static Singleton singleton;  
	    private Singleton (){}  
	    public static Singleton getSingleton() {  
	    if (singleton == null) {  
	        synchronized (Singleton.class) {  
	        if (singleton == null) {  
	            singleton = new Singleton();  
	        }  
	        }  
	    }  
	    return singleton;  
	    }  
	}  

**第三种**：饿汉式

	public class Singleton {  
	    private static Singleton instance = new Singleton();  
	    private Singleton (){}  
	    public static Singleton getInstance() {  
		    return instance;  
	    }  
	}  

**第四种**：饿汉式变种

	public class Singleton {  
	    private Singleton instance = null;  
	    static {  
	    instance = new Singleton();  
	    }  
	    private Singleton (){}  
	    public static Singleton getInstance() {  
	    return this.instance;  
	    }  
	}  

## 结构型模式

### 适配器模式
适配器模式把一个类的接口变化成以一种接口，让原来接口不匹配而无法在一起工作的两个类能够在一起工作。

![这里写图片描述](http://img.blog.csdn.net/20171102211224543?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
@适配器模式示意图（参考pattern5)

### 装饰器模式
装饰器模式是动态给一个对象添加额外的职责

![这里写图片描述](http://img.blog.csdn.net/20171102211353419?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

@装饰模式示意图（参考pattern6)

### 代理模式
代理(Proxy)是一种设计模式,提供了对目标对象另外的访问方式;即通过代理对象访问目标对象.这样做的好处是:可以在目标对象实现的基础上,增强额外的功能操作,即扩展目标对象的功能.

![这里写图片描述](http://img.blog.csdn.net/20171102211432163?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvZGF3YW5nYW5iYW4=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

	/** 
	 * 抽象角色 
	 * @author Administrator 
	 * 
	 */  
	abstract public class Subject {  
	    abstract public void request();  
	}  


	/** 
	 * 真实角色 
	 */  
	public class RealSubject extends Subject{  
	    public RealSubject(){  
          
	    }  
      
	    public void request(){  
	        System.out.println("From real subject");  
	    }  
	}  

  
	/** 
	 *  
	 * 代理方法
	 * 
	 */  
	public class ProxySubject extends Subject{  
	    private RealSubject realSubject; //以真实角色作为代理角色的属性  
      
	    public ProxySubject(){  
	          
	    }  
	  
	    @Override  
	    public void request() {  
	        preRequest();  
	          
	        if(realSubject == null){  
	            realSubject = new RealSubject();  
	        }  
	        realSubject.request();  
	          
	        postRequest();  
	    }  
	      
	    private void preRequest(){  
	        //something you want to do before requesting   
	    }  
	      
	    private void postRequest(){  
	        //something you want to do after requesting   
	    }  
  
	}  
